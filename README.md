# OpenML dataset: Human-Memory-and-Cognition

https://www.openml.org/d/43596

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Models of human cognition hold that information processing occurs in a series of stages. Cognitive psychology, in particular, is concerned with the internal mental processes that begin with the appearance of an external stimulus and result in a behavioural response.
Content
Explore human cognitive processes around the generation of narrativeswith a focus on the language employed in stories about events that have been experienced versus imagined. Investigate and characterize cognitive processes involved in storytelling, contrasting imagination and recollection of events with the help of Data Science.
Build a machine learning model that would help you to categorize cognitive processes involved in storytelling - Imagined, Recalledor Retold.
These are the columns in the data:

AssignmentId: Unique ID of this story
WorkTimeInSeconds: Time in seconds that it took the worker to do the entire HIT (reading instructions, story writing, questions)
WorkerId: Unique ID of the worker (random string, not MTurk worker ID)
annotatorAge: Lower limit of the age bucket of the worker. Buckets are: 18-24, 25-29, 30-34, 35-39, 40-44, 45-49, 50-54, 55+
annotatorGender: Gender of the worker
annotatorRace: Race/ethnicity of the worker
distracted: How distracted were you while writing your story? (5-point Likert)
draining: How taxing/draining was writing for you emotionally? (5-point Likert)
frequency: How often do you think about or talk about this event? (5-point Likert)
importance: How impactful, important, or personal is this story/event to you? (5-point Likert)
logTimeSinceEvent: Log of time (days) since the recalled event happened
mainEvent: Short phrase describing the main event described
memType: Type of story (recalled, imagined, retold) - The target variable
mostSurprising: Short phrase describing what the most surprising aspect of the story was
openness: Continuous variable representing the openness to experience of the worker
recAgnPairId: ID of the recalled story that corresponds to this retold story (null for imagined stories). Group on this variable to get the recalled-retold pairs.
recImgPairId: ID of the recalled story that corresponds to this imagined story (null for retold stories). Group on this variable to get the recalled-imagined pairs.
similarity: How similar to your life does this event/story feel to you? (5-point Likert)
similarityReason: Free text annotation of similarity
story: Story about the imagined or recalled event (15-25 sentences)
stressful: How stressful was this writing task? (5-point Likert)
summary: Summary of the events in the story (1-3 sentences)
timeSinceEvent: Time (number of days) since the recalled event happened

Likert scaling is a bipolar scaling method, measuring either positive or negative response to a statement.
Acknowledgements
Maarten Sap, Eric Horvitz, Yejin Choi, Noah A. Smith, and James Pennebaker (2020) Recollection versus Imagination: Exploring Human Memory and Cognition via Neural Language Models. ACL.
Inspiration
Explore the human cognitive process using machine learning.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43596) of an [OpenML dataset](https://www.openml.org/d/43596). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43596/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43596/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43596/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

